package com.example.seabreeze;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeabreezeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeabreezeApplication.class, args);
	}

}
