export interface ISignInRequest {
  user: {
    email: string
    password: string
  }
}
