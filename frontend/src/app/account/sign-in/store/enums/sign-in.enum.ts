export enum signInActionTypes {
    SIGN_IN = '[Auth] Sign in',
    SIGN_IN_SUCCESS = '[Auth] Sign in success',
    SIGN_IN_FAIL = '[Auth] Sign in fail'
}
