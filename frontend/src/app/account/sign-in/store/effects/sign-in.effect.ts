import {Injectable} from '@angular/core'
import {createEffect, Actions, ofType, Effect} from '@ngrx/effects'
import {map, catchError, switchMap, tap, mergeMap} from 'rxjs/operators'
import {HttpErrorResponse} from '@angular/common/http'
import {Router} from '@angular/router'
import {Observable, of} from 'rxjs'

import {ICurrentUser} from 'src/app/shared/types/currentUser.interface'
import {PersistanceService} from 'src/app/shared/services/persistance.service'

import { AccountService } from 'src/app/account/services/account.service'
import { Action } from '@ngrx/store'
import { signInActionTypes } from '../enums/sign-in.enum'
import { signIn, signInFail, signInSuccess } from './../actions/sign-in.action'


@Injectable()
export class LoginEffect {
  constructor(
    private actions$: Actions,
    private authService: AccountService,
    private persistanceService: PersistanceService,
    private router: Router
  ) {}

  /* Метод для более старых версий NGRX
  @Effect()
  loadOrders$: Observable<Action> = this.actions$.pipe(
    ofType<signInActions.signIn>(signInActionTypes.SIGN_IN),
    mergeMap((request) =>
      this.authService.singIn(request.request).pipe(
        map((currentUser: ICurrentUser) => {
          this.persistanceService.set('accessToken', currentUser.token);
          return new signInActions.signInSuccess(currentUser);
        }),

        catchError((errorResponse: HttpErrorResponse) => {
          return of(
            new signInActions.signInFail({ errors: errorResponse.error.errors })
          );
        })
      )
    )
  ); */

  login$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType<signIn>(signInActionTypes.SIGN_IN),
      switchMap((request: signIn) =>
        this.authService.singIn(request.request).pipe(
          map((currentUser: ICurrentUser) => {
            this.persistanceService.set('accessToken', currentUser.token);
            return new signInSuccess(currentUser);
          }),

          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              new signInFail({
                errors: errorResponse.error.errors,
              })
            );
          })
        )
      )
    )
  );

  redirectAfterSubmit$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(signInActionTypes.SIGN_IN_SUCCESS),
        tap(() => {
          this.router.navigateByUrl('/');
        })
      ),
    { dispatch: false }
  );
}
