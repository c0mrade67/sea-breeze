import { Action } from "@ngrx/store";

import { IBackendErrors } from "src/app/shared/types/backendErrors.interface";
import { ICurrentUser } from "src/app/shared/types/currentUser.interface";
import { ISignInRequest } from "../../domain/signInRequest.interface";
import { signInActionTypes } from "../enums/sign-in.enum";

export class signIn implements Action {
    readonly type = signInActionTypes.SIGN_IN;
  
    constructor(public request: ISignInRequest) {}
}
  
export class signInSuccess implements Action {
    readonly type = signInActionTypes.SIGN_IN_SUCCESS;
  
    constructor(public currentUser: ICurrentUser) {}
}
  
export class signInFail implements Action {
    readonly type = signInActionTypes.SIGN_IN_FAIL;
  
    constructor(public errors: IBackendErrors) {}
}

export type signInActions = 
 | signIn 
 | signInSuccess 
 | signInFail;