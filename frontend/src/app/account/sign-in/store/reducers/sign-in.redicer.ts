import { signInActions } from './../actions/sign-in.action';
import { IAuthState } from 'src/app/account/domain/authState.interface';
import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { signInActionTypes } from '../enums/sign-in.enum';

export const orderAdapter: EntityAdapter<IAuthState> = createEntityAdapter<IAuthState>();

const initialState: IAuthState = {
  isSubmitting: false,
  isLoading: false,
  currentUser: null,
  validationErrors: null,
  isLoggedIn: null
}

export function signInReducer(
  state = initialState,
  action: signInActions
): IAuthState {
  switch (action.type) {
    case signInActionTypes.SIGN_IN: {
      return {
        ...state,
        isSubmitting: true,
        validationErrors: null
      };
    }
    case signInActionTypes.SIGN_IN_SUCCESS: {
      return {
        ...state,
        isSubmitting: false,
        isLoggedIn: true,
        currentUser: action.currentUser
      };
    }
    case signInActionTypes.SIGN_IN_FAIL: {
      return {
        ...state,
        isSubmitting: false,
        validationErrors: action.errors
      };
    }

    default: {
      return state;
    }
  }
}
