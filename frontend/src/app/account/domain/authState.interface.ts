import {ICurrentUser} from 'src/app/shared/types/currentUser.interface'
import {IBackendErrors} from 'src/app/shared/types/backendErrors.interface'

export interface IAuthState {
  isSubmitting: boolean;
  currentUser: ICurrentUser | null;
  isLoggedIn: boolean | null;
  validationErrors: IBackendErrors | null;
  isLoading: boolean;
}
