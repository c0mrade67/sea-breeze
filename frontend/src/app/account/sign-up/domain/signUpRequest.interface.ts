export interface ISingUpRequest {
  user: {
    email: string
    password: string
    username: string
  }
}
