import { Action } from "@ngrx/store";

import { IBackendErrors } from "src/app/shared/types/backendErrors.interface";
import { ICurrentUser } from "src/app/shared/types/currentUser.interface";
import { ISingUpRequest } from "src/app/account/sign-up/domain/signUpRequest.interface";

import { signUpActionTypes } from "../enums/sign-up.enum";

export class signUp implements Action {
    readonly type = signUpActionTypes.SIGN_UP;
  
    constructor(public request: ISingUpRequest) {}
}
  
export class signUpSuccess implements Action {
    readonly type = signUpActionTypes.SIGN_UP_SUCCESS;
  
    constructor(public currentUser: ICurrentUser) {}
}
  
export class signUpFail implements Action {
    readonly type = signUpActionTypes.SIGN_UP_FAIL;
  
    constructor(public errors: IBackendErrors) {}
}

export type signUpActions = 
 | signUp 
 | signUpSuccess 
 | signUpFail;