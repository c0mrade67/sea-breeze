import { IAuthState } from "src/app/account/domain/authState.interface";

export interface AppStateInterface {
  auth: IAuthState
}
